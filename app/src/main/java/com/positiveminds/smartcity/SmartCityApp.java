package com.positiveminds.smartcity;

import android.app.Application;

/**
 * Created by rajeevtomar on 09/03/19.
 */

public class SmartCityApp extends Application {

    private static SmartCityApp instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

    }

    public static SmartCityApp getAppInstance()
    {
        if( instance == null)
            throw new NullPointerException("SmartCity instance is null");
        return instance;
    }

}
