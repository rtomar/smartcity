
package com.positiveminds.smartcity.base;

import android.databinding.ObservableBoolean;


import com.positiveminds.smartcity.SingleLiveEvent;
import com.positiveminds.smartcity.exception.AppException;
import com.positiveminds.smartcity.utils.rx.SchedulerProvider;

import io.reactivex.disposables.CompositeDisposable;


public abstract class BaseViewModel<N> {

    private N mNavigator;
    private final SchedulerProvider mSchedulerProvider;
    private CompositeDisposable mCompositeDisposable;
    private final SingleLiveEvent<AppException> appExceptionEvent = new SingleLiveEvent<>();
    private final ObservableBoolean mIsLoading = new ObservableBoolean(false);



    public BaseViewModel(SchedulerProvider schedulerProvider) {
        this.mSchedulerProvider = schedulerProvider;
        mCompositeDisposable = new CompositeDisposable();
    }




    public void setNavigator(N navigator) {
        this.mNavigator = navigator;
    }

    public N getNavigator() {
        return mNavigator;
    }


    public ObservableBoolean getIsLoading() {
        return mIsLoading;
    }

    public SchedulerProvider getSchedulerProvider() {
        return mSchedulerProvider;
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public SingleLiveEvent<AppException> getAppExceptionEvent()
    {
        return appExceptionEvent;
    }


    public void clearDisposable()
    {
        if(mCompositeDisposable != null)
            mCompositeDisposable.clear();
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.set(isLoading);
    }


    protected void handleError(Throwable error)
    {
        if(error == null)
            return;
        appExceptionEvent.setValue(new AppException(error.getMessage()));
    }
}
