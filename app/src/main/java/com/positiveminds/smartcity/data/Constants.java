package com.positiveminds.smartcity.data;

/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public interface Constants {

    String PREF_INTRO_DONE = "pref_show_intro";
    String PREF_SEND_OTP = "pref_send_otp";
    String PREF_PASSWORD_CREATED = "pref_pass_created";
    int AADHAR_CARD_NO_LENGTH = 12;
    int PIN_SIZE = 4;

    String RESPONSE_MSG_EMPTY = "Something went wrong!";

}
