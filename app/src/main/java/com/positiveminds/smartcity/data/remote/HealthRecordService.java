package com.positiveminds.smartcity.data.remote;

import com.positiveminds.smartcity.health.model.HealthRecordResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HealthRecordService {

    @GET("health/get")
    Single<HealthRecordResponse> getHealthRecords(@Query("customer") String userId);


}
