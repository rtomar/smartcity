package com.positiveminds.smartcity.data.remote;

import com.positiveminds.smartcity.health.model.HealthRecordResponse;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ParkingSlotService {

    @GET("Category.php?categories")
    Single<ParkingSlotResponse> getParkingSlotList(@Query("userId") String userId,
                                               @Query("latitude") double lat,
                                                   @Query("longitude") double lng);

}
