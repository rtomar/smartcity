package com.positiveminds.smartcity.health;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.utils.ActivityUtils;

import java.lang.ref.WeakReference;


public class HealthRecordActivity extends AppCompatActivity {


    public static Intent getInstance(WeakReference<Context> weakReference)
    {
        Intent intent = new Intent(weakReference.get(), HealthRecordActivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_health_record);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addHealthRecordFragment();
    }


    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }


    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void addHealthRecordFragment() {
        Fragment healthRecordFragment = obtainHealthRecordFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(),
                healthRecordFragment, R.id.container_health_record);
    }

    private HealthRecordFragment obtainHealthRecordFragment() {
        HealthRecordFragment healthRecordFragment = (HealthRecordFragment) getSupportFragmentManager().
                findFragmentById(R.id.container_health_record);
        if (healthRecordFragment == null)
            healthRecordFragment = HealthRecordFragment.newInstance();
        return healthRecordFragment;
    }


}

