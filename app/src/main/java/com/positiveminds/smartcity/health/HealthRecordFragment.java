package com.positiveminds.smartcity.health;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseFragment;
import com.positiveminds.smartcity.databinding.FragmentHealthRecordBinding;
import com.positiveminds.smartcity.databinding.FragmentLoginBinding;
import com.positiveminds.smartcity.health.viewmodel.HealthRecordViewModel;
import com.positiveminds.smartcity.login.LoginNavigator;
import com.positiveminds.smartcity.login.viewmodel.LoginViewModel;
import com.positiveminds.smartcity.utils.rx.AppSchedulerProvider;


public class HealthRecordFragment extends BaseFragment<FragmentHealthRecordBinding, HealthRecordViewModel>
        implements HealthRecordNavigator {

    //----------------------------------------------------------------------------------------------
    // Class Variabels
    //----------------------------------------------------------------------------------------------
    private HealthRecordViewModel mHealthRecordViewModel;


    public static HealthRecordFragment newInstance() {
        HealthRecordFragment fragment = new HealthRecordFragment();
        return fragment;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // get Health Record
        // As of now hardcoded userId -> 101 .... In future it will be get once login process is done
        // passing the AADHAR no. is dangerous as userId
        mHealthRecordViewModel.getHealthRecordsFromCloud("101");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //clear disposable
        mHealthRecordViewModel.clearDisposable();

    }

    //----------------------------------------------------------------------------------------------
    // BaseFragment abstract methods
    //----------------------------------------------------------------------------------------------
    @Override
    public HealthRecordViewModel getViewModel() {
        mHealthRecordViewModel = new HealthRecordViewModel(new AppSchedulerProvider());
        // set navigator
        mHealthRecordViewModel.setNavigator(this);
        return mHealthRecordViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_health_record;
    }


}
