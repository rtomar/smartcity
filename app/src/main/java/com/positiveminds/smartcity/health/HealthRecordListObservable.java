package com.positiveminds.smartcity.health;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.health.model.HealthRecord;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class HealthRecordListObservable extends BaseObservable {

    private HealthRecyclerViewAdapter mAdapter;
    private List<HealthRecord> healthRecordList;
    private boolean listEmpty;



    public HealthRecordListObservable() {
        this.mAdapter = new HealthRecyclerViewAdapter();
        healthRecordList = new ArrayList<>();
    }

    @Bindable
    public HealthRecyclerViewAdapter getAdapter() {
        return mAdapter;
    }


    @Bindable
    public List<HealthRecord> getHealthRecordList()
    {
        return healthRecordList;
    }


    public void setHealthRecordList(List<HealthRecord> healthRecordList)
    {
        this.healthRecordList = healthRecordList;
        // update the Data
        notifyPropertyChanged(BR.healthRecordList);
        notifyPropertyChanged(BR.listEmpty);
    }

    @Bindable
    public boolean isListEmpty() {
        listEmpty  = healthRecordList == null || healthRecordList.size() == 0;
        return listEmpty;
    }

}
