package com.positiveminds.smartcity.health;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.databinding.ViewHealthrecordListitemBinding;
import com.positiveminds.smartcity.health.model.HealthListItemData;
import com.positiveminds.smartcity.health.model.HealthRecord;

import java.util.ArrayList;
import java.util.List;



public class HealthRecyclerViewAdapter extends RecyclerView.Adapter<HealthRecyclerViewAdapter.DataViewHolder> {

    private List<HealthRecord> mHealthRecordList;

    public HealthRecyclerViewAdapter() {
        this.mHealthRecordList = new ArrayList<>();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_healthrecord_listitem,
                new FrameLayout(parent.getContext()), false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        HealthRecord dataModel = mHealthRecordList.get(position);
        holder.setViewModel(new HealthListItemData(dataModel));
    }

    @Override
    public void onViewAttachedToWindow(DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<HealthRecord> data) {
        if (mHealthRecordList != null)
            mHealthRecordList.clear();
        mHealthRecordList = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(mHealthRecordList == null)
            return 0;
        return mHealthRecordList.size();
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        ViewHealthrecordListitemBinding binding;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

         void setViewModel(HealthListItemData viewModel) {
            if (binding != null) {
                binding.setItemData(viewModel);
            }
        }
    }
}
