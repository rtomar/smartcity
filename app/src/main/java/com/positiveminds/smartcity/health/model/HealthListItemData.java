package com.positiveminds.smartcity.health.model;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class HealthListItemData{

    private HealthRecord healthRecordData;
    private Integer viewBackgroundDrawableId;

    public HealthListItemData(HealthRecord healthRecordData)
    {
        this.healthRecordData = healthRecordData;
    }

    public HealthRecord getHealthRecordData() {
        return healthRecordData;
    }
}
