package com.positiveminds.smartcity.health.model;

import com.google.gson.annotations.SerializedName;

public class HealthRecord {

    @SerializedName("disease")
    String disease;

    @SerializedName("date")
    String date;

    @SerializedName("doctorFirstName")
    String docFirstName;

    @SerializedName("doctorLastName")
    String docLastName;

    @SerializedName("doctorRating")
    int docRating;

    @SerializedName("consultationId")
    int consultationId;


    public String getDisease() {
        return disease;
    }

    public String getDate() {
        return date;
    }

    public String getDocFirstName() {
        return docFirstName;
    }

    public String getDocLastName() {
        return docLastName;
    }

    public int getDocRating() {
        return docRating;
    }

    public int getConsultationId() {
        return consultationId;
    }


    public void setDisease(String disease) {
        this.disease = disease;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setDocFirstName(String docFirstName) {
        this.docFirstName = docFirstName;
    }

    public void setDocLastName(String docLastName) {
        this.docLastName = docLastName;
    }

    public void setDocRating(int docRating) {
        this.docRating = docRating;
    }

    public void setConsultationId(int consultationId) {
        this.consultationId = consultationId;
    }

}
