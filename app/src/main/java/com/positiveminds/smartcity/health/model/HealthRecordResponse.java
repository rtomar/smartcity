package com.positiveminds.smartcity.health.model;

import com.google.gson.annotations.SerializedName;
import com.positiveminds.smartcity.server.BaseResponse;

import java.util.List;

public class HealthRecordResponse extends BaseResponse {

    @SerializedName("results")
   List<HealthRecord> healthRecordList;

    public List<HealthRecord> getHealthRecordList() {
        return healthRecordList;
    }
}
