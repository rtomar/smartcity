package com.positiveminds.smartcity.health.viewmodel;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseViewModel;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.data.remote.HealthRecordService;
import com.positiveminds.smartcity.exception.AppException;
import com.positiveminds.smartcity.health.HealthRecordListObservable;
import com.positiveminds.smartcity.health.HealthRecordNavigator;
import com.positiveminds.smartcity.health.HealthRecyclerViewAdapter;
import com.positiveminds.smartcity.health.model.HealthRecord;
import com.positiveminds.smartcity.health.model.HealthRecordResponse;
import com.positiveminds.smartcity.server.RestManager;
import com.positiveminds.smartcity.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class HealthRecordViewModel extends BaseViewModel<HealthRecordNavigator> {


    private HealthRecordListObservable healthRecordListObservable;

    public HealthRecordViewModel(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
        healthRecordListObservable = new HealthRecordListObservable();
    }


    public HealthRecordListObservable getHealthRecordListObservable() {
        return healthRecordListObservable;
    }

    public void getHealthRecordsFromCloud(String userId) {
        setIsLoading(true);
        HealthRecordService productService = RestManager.getRetrofitClient().create(HealthRecordService.class);
        Disposable disposable = productService.getHealthRecords(userId).subscribeOn(getSchedulerProvider().io()).
                observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<HealthRecordResponse>() {
                                                                     @Override
                                                                     public void accept(HealthRecordResponse healthRecordResponse) throws Exception {
                                                                         setIsLoading(false);
                                                                         if (healthRecordResponse != null) {
//                                                                             List<HealthRecord> healthRecords = new ArrayList<>();
//                                                                             HealthRecord healthRecord = new HealthRecord();
//                                                                             healthRecord.setDate("09/03/2019");
//                                                                             healthRecord.setDocFirstName("Dr. Ramesh Singh");
//                                                                             healthRecord.setDisease("Fever");
//                                                                             healthRecords.add(healthRecord);
//                                                                             displayHealthRecords(healthRecords);
//                                                                             return;
                                                                             boolean status = healthRecordResponse.getStatus();
                                                                             if (!status) // true -> status
                                                                             {
                                                                                 String message = healthRecordResponse.getMessage();
                                                                                 if (TextUtils.isEmpty(message))
                                                                                     message = Constants.RESPONSE_MSG_EMPTY;
                                                                                 getAppExceptionEvent().setValue(new AppException(message));
                                                                                 return;
                                                                             }
                                                                             displayHealthRecords(healthRecordResponse.getHealthRecordList());

                                                                         }
                                                                     }
                                                                 }, new Consumer<Throwable>() {
                                                                     @Override
                                                                     public void accept(Throwable throwable) throws Exception {
                                                                         setIsLoading(false);
                                                                         getAppExceptionEvent().setValue(new AppException(throwable.getMessage()));
                                                                     }
                                                                 }
        );
        // add to disposable
        getCompositeDisposable().add(disposable);

    }



    //----------------------------------------------------------------------------------------------
    // BindingAdapters methods
    //----------------------------------------------------------------------------------------------
    @BindingAdapter({"app:adapter", "app:data"})
    public static void bind(RecyclerView recyclerView, HealthRecyclerViewAdapter adapter,
                            List<HealthRecord> healthRecordList) {
        recyclerView.setAdapter(adapter);
        // update adapter
        adapter.updateData(healthRecordList);

    }



    private void displayHealthRecords(List<HealthRecord> healthRecordList) {
        if (healthRecordList == null || healthRecordList.size() == 0) {
            getAppExceptionEvent().setValue(new AppException(R.string.health_record_not_found));
            return;
        }
        healthRecordListObservable.setHealthRecordList(healthRecordList);
    }

}
