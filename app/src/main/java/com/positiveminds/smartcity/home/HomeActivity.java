package com.positiveminds.smartcity.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseActivity;
import com.positiveminds.smartcity.utils.ActivityUtils;

import java.lang.ref.WeakReference;



public class HomeActivity extends BaseActivity {


    public static Intent getNewIntent(WeakReference<Context> weakReference)
    {
        Intent intent = new Intent(weakReference.get(),HomeActivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        addHomeFragment();
    }

    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void addHomeFragment()
    {
        Fragment homeFragment = obtainLoginFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(),
                homeFragment,R.id.container_home);
    }

    private HomeFragment obtainLoginFragment()
    {
        HomeFragment homeFragment = (HomeFragment) getSupportFragmentManager().
                findFragmentById(R.id.container_home);
        if(homeFragment == null)
            homeFragment = HomeFragment.getInstance();
        return homeFragment;
    }


}
