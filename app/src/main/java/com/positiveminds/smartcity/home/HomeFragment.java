package com.positiveminds.smartcity.home;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;

import com.positiveminds.smartcity.BR;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseFragment;
import com.positiveminds.smartcity.databinding.FragmentHomeBinding;
import com.positiveminds.smartcity.health.HealthRecordActivity;
import com.positiveminds.smartcity.home.viewmodel.HomeViewModel;
import com.positiveminds.smartcity.transport.parking.ParkingSlotActivity;
import com.positiveminds.smartcity.utils.rx.AppSchedulerProvider;

import java.lang.ref.WeakReference;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends BaseFragment<FragmentHomeBinding, HomeViewModel>
        implements HomeNavigator {

    private HomeViewModel mHomeViewModel;


    public static HomeFragment getInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mHomeViewModel.getUserInfoFromCloud();
    }

    //----------------------------------------------------------------------------------------------
    // BaseFragment abstract methods
    //----------------------------------------------------------------------------------------------
    @Override
    public HomeViewModel getViewModel() {
        mHomeViewModel = new HomeViewModel(new AppSchedulerProvider());
        // set navigator
        mHomeViewModel.setNavigator(this);
        return mHomeViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }


    //----------------------------------------------------------------------------------------------
    // HomeNavigator implemented methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void moveToHealthRecordScreen() {
        // launch Health Record Screen
        Intent intent = HealthRecordActivity.getInstance(new WeakReference<Context>(getContext()));
        startActivity(intent);
    }

    @Override
    public void moveToTransportScreen() {
        // launch Parking Slot Screen
        Intent intent = ParkingSlotActivity.getNewIntent(new WeakReference<Context>(getContext()));
        startActivity(intent);

    }


    @Override
    public void moveToSecurityScreen() {

    }
}
