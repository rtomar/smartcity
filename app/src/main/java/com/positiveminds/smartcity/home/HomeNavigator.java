package com.positiveminds.smartcity.home;

/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public interface HomeNavigator {

    void moveToHealthRecordScreen();
    void moveToTransportScreen();
    void moveToSecurityScreen();
}
