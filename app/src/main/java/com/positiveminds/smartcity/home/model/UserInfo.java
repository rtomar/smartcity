package com.positiveminds.smartcity.home.model;

import android.databinding.ObservableField;

/**
 * Created by Rajeev Tomar on 01/03/19.
 */
public class UserInfo {

    private ObservableField<String> name = new ObservableField<>();
    private ObservableField<String> userPicUlr = new ObservableField<>();


    public void setName(String name) {
        this.name.set(name);
    }


    public void setUserPicUlr(String userPicUlr) {
        this.userPicUlr.set(userPicUlr);
    }


    public ObservableField<String> getName() {
        return name;
    }

    public ObservableField<String> getUserPicUlr() {
        return userPicUlr;
    }

}
