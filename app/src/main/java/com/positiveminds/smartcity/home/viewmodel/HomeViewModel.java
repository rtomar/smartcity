package com.positiveminds.smartcity.home.viewmodel;


import com.positiveminds.smartcity.base.BaseViewModel;
import com.positiveminds.smartcity.home.HomeNavigator;
import com.positiveminds.smartcity.home.model.UserInfo;
import com.positiveminds.smartcity.utils.rx.SchedulerProvider;

/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class HomeViewModel extends BaseViewModel<HomeNavigator> {

    private UserInfo mUserInfo = new UserInfo();


    public HomeViewModel(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
    }


    //----------------------------------------------------------------------------------------------
    // View-ViewModel Interaction methods
    //----------------------------------------------------------------------------------------------
    public void getUserInfoFromCloud()
    {
        updateDashboardData();

    }

    //----------------------------------------------------------------------------------------------
    // View_DataBinding methods
    //----------------------------------------------------------------------------------------------
    public UserInfo getUserInfo() {
        return mUserInfo;
    }

    public void onClickHealthRecord()
    {
        getNavigator().moveToHealthRecordScreen();
    }

    public void onClickTransport()
    {
        getNavigator().moveToTransportScreen();
    }

    public void onClickSecurity()
    {
        getNavigator().moveToSecurityScreen();
    }



    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void updateDashboardData()
    {
        mUserInfo.setName("Hello, Rajeev Tomar");
    }

}
