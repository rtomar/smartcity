package com.positiveminds.smartcity.intro;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.positiveminds.smartcity.R;

import agency.tango.materialintroscreen.SlideFragment;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class AttendanceCustomSlide extends SlideFragment {

    private static final String ARG_ICON = "arg_icon";
    private static final String ARG_TITLE = "arg_title";
    private static final String ARG_DESC = "arg_desc";
    private static final String ARG_BGC = "arg_bg";
    private static final String ARG_BC = "arg_bc";


    private int mBackgroundColor;
    private int mButtonColor;

    public static AttendanceCustomSlide getNewInstance(int drawable, String title, String description,
                                               int backgroundColor, int buttonColor)
    {
        AttendanceCustomSlide slideFragment = new AttendanceCustomSlide();
        Bundle bundle = new Bundle();
        bundle.putInt(ARG_ICON, drawable);
        bundle.putString(ARG_TITLE, title);
        bundle.putString(ARG_DESC, description);
        bundle.putInt(ARG_BGC,backgroundColor);
        bundle.putInt(ARG_BC,buttonColor);
        slideFragment.setArguments(bundle);
        return slideFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_intro, container, false);
        setBundleData(view);
        return view;
    }


    private void setBundleData(View view)
    {
        Bundle bundle = getArguments();
        if(bundle == null)
            return;
        mBackgroundColor = bundle.getInt(ARG_BGC);
        mButtonColor = bundle.getInt(ARG_BC);
        int drawableId = bundle.getInt(ARG_ICON);
        String title = bundle.getString(ARG_TITLE);
        String description = bundle.getString(ARG_DESC);

        if(drawableId > 0)
        {
            ImageView imageView = view.findViewById(R.id.iv_attendance);
            imageView.setImageDrawable(getResources().getDrawable(drawableId));
        }
        if(!TextUtils.isEmpty(title))
        {
            TextView titleView = view.findViewById(R.id.title);
            titleView.setText(title);
        }
        if(!TextUtils.isEmpty(description))
        {
            TextView descView = view.findViewById(R.id.desc);
            descView.setText(description);
        }

    }

    @Override
    public int backgroundColor() {
        return mBackgroundColor;
    }

    @Override
    public int buttonsColor() {
        return mButtonColor;
    }

    @Override
    public boolean canMoveFurther() {
        return true;
    }

    @Override
    public String cantMoveFurtherErrorMessage() {
        return getString(R.string.intro_error);
    }

}
