package com.positiveminds.smartcity.intro;

import android.content.Intent;
import android.os.Bundle;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.login.LoginActivity;
import com.positiveminds.smartcity.utils.PreferencesUtils;

import agency.tango.materialintroscreen.MaterialIntroActivity;


public class IntroActivity extends MaterialIntroActivity {

    //----------------------------------------------------------------------------------------------
    // lifecycle methods
    //----------------------------------------------------------------------------------------------
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addSlides();
    }


    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void addSlides() {
        // Add attendance Slide
        addSlide(AttendanceCustomSlide.getNewInstance(R.drawable.ic_health,
                getResources().getString(R.string.health_title),
                getResources().getString(R.string.health_desc),
                R.color.white,R.color.colorAccent));

//        // Add HomeWork Slide
//        addSlide(AttendanceCustomSlide.getNewInstance(R.drawable.ic_homework,
//                getResources().getString(R.string.homework_title),
//                getResources().getString(R.string.homework_desc),
//                R.color.white,R.color.light_pink));



        // Add Notification Slide
        addSlide(AttendanceCustomSlide.getNewInstance(R.drawable.ic_transport,
                getResources().getString(R.string.transport_title),
                getResources().getString(R.string.transport_desc),
                R.color.white,R.color.dark_yellow));


        // Add Result Slide
        addSlide(AttendanceCustomSlide.getNewInstance(R.drawable.ic_security,
                getResources().getString(R.string.security_title),
                getResources().getString(R.string.transport_desc),
                R.color.white,R.color.colorPrimary));
    }


    //----------------------------------------------------------------------------------------------
    // Parent Material overridden methods
    //----------------------------------------------------------------------------------ß------------
    @Override
    public void onFinish() {
        super.onFinish();
        // set preference true to hide intro for next launch
        PreferencesUtils.putPref(Constants.PREF_INTRO_DONE,true);
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
