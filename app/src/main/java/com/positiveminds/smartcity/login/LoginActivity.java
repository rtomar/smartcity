package com.positiveminds.smartcity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.home.HomeActivity;
import com.positiveminds.smartcity.utils.ActivityUtils;

import java.lang.ref.WeakReference;


/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends AppCompatActivity implements LoginFragment.
        OnFragmentInteractionListener{


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        addLoginFragment();
    }

    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void addLoginFragment()
    {
        Fragment loginFragment = obtainLoginFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(),
                loginFragment,R.id.login_container);
    }

    private LoginFragment obtainLoginFragment()
    {
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().
                findFragmentById(R.id.login_container);
        if(loginFragment == null)
            loginFragment = LoginFragment.newInstance();
        return loginFragment;
    }

    //----------------------------------------------------------------------------------------------
    // HealthRecordFragment.OnFragmentInteractionListener implemented methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void onLoginSuccessfully() {
        Intent intent = HomeActivity.getNewIntent(new WeakReference<Context>(this));
        startActivity(intent);
        finish();
    }
}

