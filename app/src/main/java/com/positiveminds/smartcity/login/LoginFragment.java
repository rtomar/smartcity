package com.positiveminds.smartcity.login;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseFragment;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.databinding.FragmentLoginBinding;
import com.positiveminds.smartcity.login.LoginNavigator;
import com.positiveminds.smartcity.login.viewmodel.LoginFormFields;
import com.positiveminds.smartcity.login.viewmodel.LoginViewModel;
import com.positiveminds.smartcity.utils.PreferencesUtils;
import com.positiveminds.smartcity.utils.rx.AppSchedulerProvider;


public class LoginFragment extends BaseFragment<FragmentLoginBinding, LoginViewModel>
        implements LoginNavigator {

    //----------------------------------------------------------------------------------------------
    // Class Variabels
    //----------------------------------------------------------------------------------------------
    private OnFragmentInteractionListener mListener;
    private LoginViewModel mLoginViewModel;


    public static LoginFragment newInstance() {
        LoginFragment fragment = new LoginFragment();
        return fragment;
    }


    //----------------------------------------------------------------------------------------------
    // BaseFragment abstract methods
    //----------------------------------------------------------------------------------------------
    @Override
    public LoginViewModel getViewModel() {
        mLoginViewModel = new LoginViewModel(new AppSchedulerProvider());
        // set navigator
        mLoginViewModel.setNavigator(this);
        return mLoginViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_login;
    }

    public interface OnFragmentInteractionListener {
        void onLoginSuccessfully();
    }

    //----------------------------------------------------------------------------------------------
    // Fragment life-cycle methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // start login process on tap keboard enter button
        //updateUI();
        EditText passwordEditText = getViewDataBinding().etPassword;
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            //            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
//                    // start validation process
                    mLoginViewModel.onClickLogin();
                    return true;
                }
                return false;
            }
        });
        captureValidatedFormLiveData();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    //----------------------------------------------------------------------------------------------
    // LoginNavigator implemented methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void onLoginCompleted() {
        //let's take decision by parent activity... As login fragment might get used by other activities
        mListener.onLoginSuccessfully();

    }

    //----------------------------------------------------------------------------------------------
    // Private methdos
    //----------------------------------------------------------------------------------------------
    public void captureValidatedFormLiveData() {
        mLoginViewModel.getValidatedFormData().observe(this, new Observer<LoginFormFields>() {
            @Override
            public void onChanged(@Nullable LoginFormFields loginFormFields) {
                // play with data if required or pass to viewmodel to proceed login process
//                boolean otpSent = PreferencesUtils.getPref(Constants.PREF_SEND_OTP, false);
//                boolean pinCreated = PreferencesUtils.getPref(Constants.PREF_PASSWORD_CREATED, false);
//                if (!otpSent) {
//                    // show otp dialog and press
//                    showOTPDialog();
//                    PreferencesUtils.putPref(Constants.PREF_SEND_OTP, true);
//                    updateUI();
//                } else if (otpSent && !pinCreated) {
//                    PreferencesUtils.putPref(Constants.PREF_PASSWORD_CREATED, true);
//                    updateUI();
//
//                } else
                    mLoginViewModel.doLogin(loginFormFields.getUserName(), loginFormFields.getPassword());
            }
        });
    }


    private void updateUI() {
        // change button text and hide fields based on perferences values
        boolean otpSent = PreferencesUtils.getPref(Constants.PREF_SEND_OTP, false);
        boolean pinCreated = PreferencesUtils.getPref(Constants.PREF_PASSWORD_CREATED, false);
        if (!otpSent) {
            // change button text
            getViewDataBinding().signin1.setText(getResources().getString(R.string.send_otp));
            // hide both input fields
            getViewDataBinding().password.setVisibility(View.GONE);
            getViewDataBinding().viewPassword.setVisibility(View.GONE);
            //getViewDataBinding().passwordConfirm.setVisibility(View.GONE);
        } else if (otpSent && !pinCreated) {
            getViewDataBinding().signin1.setText(getResources().getString(R.string.create_pin));
            getViewDataBinding().password.setVisibility(View.VISIBLE);
            getViewDataBinding().viewPassword.setVisibility(View.VISIBLE);
            getViewDataBinding().email1.setVisibility(View.GONE);
            getViewDataBinding().viewUser.setVisibility(View.GONE);
            getViewDataBinding().passwordConfirm.setVisibility(View.VISIBLE);
            getViewDataBinding().viewConfirm.setVisibility(View.VISIBLE);
            getViewDataBinding().otp.setVisibility(View.VISIBLE);
            getViewDataBinding().viewOtp.setVisibility(View.VISIBLE);

        } else {
            getViewDataBinding().signin1.setText(getResources().getString(R.string.action_sign_in_short));
            getViewDataBinding().password.setVisibility(View.VISIBLE);
            getViewDataBinding().viewPassword.setVisibility(View.VISIBLE);
            getViewDataBinding().email1.setVisibility(View.VISIBLE);
            getViewDataBinding().viewUser.setVisibility(View.VISIBLE);
            getViewDataBinding().passwordConfirm.setVisibility(View.GONE);
            getViewDataBinding().viewConfirm.setVisibility(View.GONE);
            getViewDataBinding().otp.setVisibility(View.GONE);
            getViewDataBinding().viewOtp.setVisibility(View.GONE);


        }
    }

    private void showOTPDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Your OTP is 1234");
        builder.setCancelable(true);

        builder.setPositiveButton(
                "OKAY",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

}
