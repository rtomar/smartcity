package com.positiveminds.smartcity.login;

/**
 * Created by Rajeev Tomar on 29/03/19.
 */
public interface LoginNavigator {

    void onLoginCompleted();
}
