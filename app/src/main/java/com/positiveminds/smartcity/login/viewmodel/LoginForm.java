package com.positiveminds.smartcity.login.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.utils.PreferencesUtils;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class LoginForm extends BaseObservable {

    private LoginFormFields loginFormFields;
    private LoginFormValidations loginFormValidations;
    private MutableLiveData<LoginFormFields> loginFormFieldsMutableLiveData;

    public LoginForm() {
        loginFormFields = new LoginFormFields();
        loginFormValidations = new LoginFormValidations();
        loginFormFieldsMutableLiveData = new MutableLiveData<>();
    }

    public LoginFormFields getLoginFormFields() {
        return loginFormFields;
    }

    public MutableLiveData<LoginFormFields> getLoginFormFieldsMutableLiveData() {
        return loginFormFieldsMutableLiveData;
    }


    public void doFormValidation()
    {
        // do validation
        if(isInputDataValid())
            loginFormFieldsMutableLiveData.setValue(loginFormFields);
    }


    private boolean isInputDataValid()
    {
        // As we can direct value form LoginFormFields class becuase of two way data binding
        String userName = loginFormFields.getUserName();
        if(TextUtils.isEmpty(userName))
        {
            // set Error
            loginFormValidations.setUserNameValidation(R.string.error_field_required);
            // to notify filed must be in BR table or bindable
            notifyPropertyChanged(BR.userValidation);
            return false;
        }

        // check length as AADHAR card can't be more than 12
        int userTextSize = userName.length();
        if(userTextSize != Constants.AADHAR_CARD_NO_LENGTH)
        {
            loginFormValidations.setUserNameValidation(R.string.error_user_length);
            notifyPropertyChanged(BR.userValidation);
            return false;
        }

//        boolean otpSent = PreferencesUtils.getPref(Constants.PREF_SEND_OTP,false);
//        boolean pinCreated = PreferencesUtils.getPref(Constants.PREF_PASSWORD_CREATED,false);

//        if(!otpSent)
//            return true;
//        if(otpSent && !pinCreated)
//        {
//            // check OTP
//            String otp = loginFormFields.getOtp();
//            if(!otp.equals("1234"))
//            {
//                loginFormValidations.setOtpValidation(R.string.error_wrong_otp);
//                notifyPropertyChanged(BR.otpValidation);
//                return false;
//            }
//        }

        String password = loginFormFields.getPassword();
        if(TextUtils.isEmpty(password))
        {
            // set Error
            loginFormValidations.setPasswordValidation(R.string.error_field_required);
            // to notify filed must be in BR table or bindable
            notifyPropertyChanged(BR.passwordValidation);
            return false;
        }

        // check size as 4 digit
        int passwordTextSize = password.length();
        if(passwordTextSize != Constants.PIN_SIZE)
        {
            loginFormValidations.setPasswordValidation(R.string.error_pin_length);
            notifyPropertyChanged(BR.passwordValidation);
            return false;
        }
//        String confirmPassword = loginFormFields.getConfirmPassword();
//        if(!password.equals(confirmPassword))
//        {
//            loginFormValidations.setConfirmPassValidation(R.string.pin_not_match);
//            notifyPropertyChanged(BR.confirmPassValidation);
//            return false;
//
//        }
        return true;
    }


    @Bindable
    public @StringRes Integer getUserValidation()
    {
        return loginFormValidations.getUserNameValidation();
    }

    @Bindable
    public @StringRes Integer getPasswordValidation()
    {
        return loginFormValidations.getPasswordValidation();
    }

    @Bindable
    public @StringRes Integer getConfirmPassValidation()
    {
        return loginFormValidations.getConfirmPassValidation();
    }

    @Bindable
    public @StringRes Integer getOtpValidation()
    {
        return loginFormValidations.getOtpValidation();
    }


}
