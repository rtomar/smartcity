package com.positiveminds.smartcity.login.viewmodel;

import android.support.annotation.StringRes;

/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class LoginFormValidations {

    private @StringRes Integer userNameValidation;
    private @StringRes Integer passwordValidation;
    private @StringRes Integer otpValidation;
    private @StringRes Integer confirmPassValidation;

    public Integer getUserNameValidation() {
        return userNameValidation;
    }

    public void setUserNameValidation(Integer userNameValidation) {
        this.userNameValidation = userNameValidation;
    }

    public Integer getPasswordValidation() {
        return passwordValidation;
    }

    public void setPasswordValidation(Integer passwordValidation) {
        this.passwordValidation = passwordValidation;
    }

    public Integer getOtpValidation() {
        return otpValidation;
    }

    public void setOtpValidation(Integer otpValidation) {
        this.otpValidation = otpValidation;
    }

    public Integer getConfirmPassValidation() {
        return confirmPassValidation;
    }

    public void setConfirmPassValidation(Integer confirmPassValidation) {
        this.confirmPassValidation = confirmPassValidation;
    }
}
