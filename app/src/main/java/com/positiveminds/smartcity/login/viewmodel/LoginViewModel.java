package com.positiveminds.smartcity.login.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BindingAdapter;
import android.widget.EditText;

import com.positiveminds.smartcity.base.BaseViewModel;
import com.positiveminds.smartcity.login.LoginNavigator;
import com.positiveminds.smartcity.utils.rx.SchedulerProvider;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class LoginViewModel extends BaseViewModel<LoginNavigator> {

    private LoginForm loginForm;

    public LoginViewModel(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
        loginForm = new LoginForm();
    }


    //----------------------------------------------------------------------------------------------
    // View-ViewModel Interaction methods
    //----------------------------------------------------------------------------------------------
    public void doLogin(String userName, String password) {
        // start login process
        if (getNavigator() != null)
            getNavigator().onLoginCompleted();

    }

    public MutableLiveData<LoginFormFields> getValidatedFormData() {
        return loginForm.getLoginFormFieldsMutableLiveData();
    }


    //----------------------------------------------------------------------------------------------
    // View_DataBinding methods
    //----------------------------------------------------------------------------------------------
    public void onClickLogin() {
        // do Validation and proceed
        loginForm.doFormValidation();
    }


    public LoginForm getLoginForm() {
        return loginForm;
    }

    @BindingAdapter("error")
    public static void setError(EditText editText, Object strOrResId) {
        if (strOrResId instanceof Integer) {
            editText.setError(
                    editText.getContext().getString((Integer) strOrResId));
        } else {
            editText.setError((String) strOrResId);
        }
    }
}
