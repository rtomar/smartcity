package com.positiveminds.smartcity.server;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rajeevtomar on 09/03/19.
 */

public class BaseResponse {

    @SerializedName("success")
    boolean status;

    @SerializedName("message")
    String message;


    public boolean getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
