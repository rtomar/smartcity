package com.positiveminds.smartcity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.intro.IntroActivity;
import com.positiveminds.smartcity.login.LoginActivity;
import com.positiveminds.smartcity.utils.PreferencesUtils;


public class SplashActivity extends AppCompatActivity {

    private static final int TIME_STAMP = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                launchNextScreen();
            }
        },TIME_STAMP);
    }

    private void launchNextScreen()
    {
        boolean isIntroDone = PreferencesUtils.getPref(Constants.PREF_INTRO_DONE,false);
        Intent intent = new Intent(this, LoginActivity.class);
        if(!isIntroDone)
        {
            intent = new Intent(this, IntroActivity.class);
        }
        startActivity(intent);
        finish();
    }
}
