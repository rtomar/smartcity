package com.positiveminds.smartcity.transport.parking;

import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.databinding.ViewHealthrecordListitemBinding;
import com.positiveminds.smartcity.databinding.ViewParkingslotListitemBinding;
import com.positiveminds.smartcity.health.model.HealthListItemData;
import com.positiveminds.smartcity.health.model.HealthRecord;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotData;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotListItemData;

import java.util.ArrayList;
import java.util.List;


public class ParkingRecyclerViewAdapter extends RecyclerView.Adapter<ParkingRecyclerViewAdapter.DataViewHolder> {

    private List<ParkingSlotData> parkingSlotDataList;

    public ParkingRecyclerViewAdapter() {
        this.parkingSlotDataList = new ArrayList<>();
    }

    @Override
    public DataViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_parkingslot_listitem,
                new FrameLayout(parent.getContext()), false);
        return new DataViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DataViewHolder holder, int position) {
        ParkingSlotData dataModel = parkingSlotDataList.get(position);
        holder.setViewModel(new ParkingSlotListItemData(dataModel));
    }

    @Override
    public void onViewAttachedToWindow(DataViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(DataViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<ParkingSlotData> data) {
        if (parkingSlotDataList != null)
            parkingSlotDataList.clear();
        parkingSlotDataList = data;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if(parkingSlotDataList == null)
            return 0;
        return parkingSlotDataList.size();
    }

    static class DataViewHolder extends RecyclerView.ViewHolder {
        ViewParkingslotListitemBinding binding;

        DataViewHolder(View itemView) {
            super(itemView);
            bind();
        }

        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind(); // Don't forget to unbind
            }
        }

         void setViewModel(ParkingSlotListItemData viewModel) {
            if (binding != null) {
                binding.setItemData(viewModel);
            }
        }
    }
}
