package com.positiveminds.smartcity.transport.parking;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.utils.ActivityUtils;

import java.lang.ref.WeakReference;

public class ParkingSlotActivity extends AppCompatActivity {


    public static Intent getNewIntent(WeakReference<Context> weakReference)
    {
        Intent intent = new Intent(weakReference.get(), ParkingSlotActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parking_slot);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        addParkingSlotFragment();
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }



    //----------------------------------------------------------------------------------------------
    // Private methods
    //---------------------------------------------------------------------------------------------
    private void addParkingSlotFragment()
    {
        Fragment parkingSlotFragment = obtainParkingSlotFragment();
        ActivityUtils.replaceFragmentInActivity(getSupportFragmentManager(),
                parkingSlotFragment,R.id.container_parking_slot);
    }

    private ParkingSlotFragment obtainParkingSlotFragment()
    {
        ParkingSlotFragment parkingSlotFragment = (ParkingSlotFragment) getSupportFragmentManager().
                findFragmentById(R.id.container_parking_slot);
        if(parkingSlotFragment == null)
            parkingSlotFragment = ParkingSlotFragment.newInstance();
        return parkingSlotFragment;
    }

}
