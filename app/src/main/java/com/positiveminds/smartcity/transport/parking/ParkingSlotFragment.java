package com.positiveminds.smartcity.transport.parking;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseFragment;

import com.positiveminds.smartcity.databinding.FragmentParkingSlotBinding;
import com.positiveminds.smartcity.exception.AppException;
import com.positiveminds.smartcity.transport.parking.viewmodel.ParkingSlotViewModel;
import com.positiveminds.smartcity.utils.rx.AppSchedulerProvider;

public class ParkingSlotFragment extends BaseFragment<FragmentParkingSlotBinding,
        ParkingSlotViewModel>  implements ParkingSlotNavigator{


    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1001;


    private FusedLocationProviderClient fusedLocationClient;
    private ParkingSlotViewModel mSlotViewModel;



    public static ParkingSlotFragment newInstance() {
        ParkingSlotFragment fragment = new ParkingSlotFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    //----------------------------------------------------------------------------------------------
    // Fragment Lifecycle methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getContext());
        checkLocationPermission();
    }


    @Override
    public void onResume() {
        super.onResume();
        // get Current location
        getCurrentLocation();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // get location and move ahead
                    getCurrentLocation();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    //----------------------------------------------------------------------------------------------
    // BaseFragment abstract methods
    //----------------------------------------------------------------------------------------------
    @Override
    public ParkingSlotViewModel getViewModel() {
        mSlotViewModel = new ParkingSlotViewModel(new AppSchedulerProvider());
        //set navigator
        mSlotViewModel.setNavigator(this);
        return mSlotViewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_parking_slot;
    }



    //----------------------------------------------------------------------------------------------
    // Private methods
    //----------------------------------------------------------------------------------------------
    private void checkLocationPermission()
    {
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_COARSE_LOCATION)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);

                // MY_PERMISSIONS_REQUEST_LOCATION is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
            // get location and show parking slots
            getCurrentLocation();
        }
    }



    private void getCurrentLocation()
    {
        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // get parking slot based on current location
                                mSlotViewModel.getParkingSlotsFromCloud(location);

                            }
                        }
                    });
        }catch (SecurityException ex)
        {
            if(mSlotViewModel != null)
            {
                mSlotViewModel.getAppExceptionEvent().setValue(new AppException(ex.getMessage()));
            }
        }
    }



    //----------------------------------------------------------------------------------------------
    // ParkingSlotNavigator implemented methods
    //----------------------------------------------------------------------------------------------
    @Override
    public void moveToSlotBookingScreen() {
        // move to slot booking screen
    }
}
