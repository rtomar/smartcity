package com.positiveminds.smartcity.transport.parking;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.positiveminds.smartcity.BR;
import com.positiveminds.smartcity.health.HealthRecyclerViewAdapter;
import com.positiveminds.smartcity.health.model.HealthRecord;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotData;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class ParkingSlotListObservable extends BaseObservable {

    private ParkingRecyclerViewAdapter mAdapter;
    private List<ParkingSlotData> parkingSlotDataList;
    private boolean listEmpty;



    public ParkingSlotListObservable() {
        this.mAdapter = new ParkingRecyclerViewAdapter();
        parkingSlotDataList = new ArrayList<>();
    }

    @Bindable
    public ParkingRecyclerViewAdapter getAdapter() {
        return mAdapter;
    }


    @Bindable
    public List<ParkingSlotData> getParkingSlotDataList()
    {
        return parkingSlotDataList;
    }


    public void setParkingSlotDataList(List<ParkingSlotData> parkingSlotData)
    {
        this.parkingSlotDataList = parkingSlotData;
        // update the Data
        notifyPropertyChanged(BR.parkingSlotDataList);
        notifyPropertyChanged(BR.listEmpty);
    }

    @Bindable
    public boolean isListEmpty() {
        listEmpty  = parkingSlotDataList == null || parkingSlotDataList.size() == 0;
        return listEmpty;
    }

}
