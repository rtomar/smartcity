package com.positiveminds.smartcity.transport.parking;

public interface ParkingSlotNavigator {

    void moveToSlotBookingScreen();

}
