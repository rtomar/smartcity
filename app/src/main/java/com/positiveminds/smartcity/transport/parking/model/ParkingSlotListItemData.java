package com.positiveminds.smartcity.transport.parking.model;


import com.positiveminds.smartcity.health.model.HealthRecord;

/**
 * Created by Rajeev Tomar on 09/03/19.
 */
public class ParkingSlotListItemData {

    private ParkingSlotData parkingSlotData;

    public ParkingSlotListItemData(ParkingSlotData parkingSlotData)
    {
        this.parkingSlotData = parkingSlotData;
    }

    public ParkingSlotData getParkingSlotData() {
        return parkingSlotData;
    }
}
