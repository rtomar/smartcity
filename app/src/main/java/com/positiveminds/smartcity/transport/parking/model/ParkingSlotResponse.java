package com.positiveminds.smartcity.transport.parking.model;

import com.positiveminds.smartcity.server.BaseResponse;

import java.util.List;

public class ParkingSlotResponse extends BaseResponse {

    List<ParkingSlotData> parkingSlotDataList;

    public List<ParkingSlotData> getParkingSlotDataList() {
        return parkingSlotDataList;
    }

    public void setParkingSlotDataList(List<ParkingSlotData> parkingSlotDataList) {
        this.parkingSlotDataList = parkingSlotDataList;
    }
}
