package com.positiveminds.smartcity.transport.parking.viewmodel;

import android.databinding.BindingAdapter;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.positiveminds.smartcity.R;
import com.positiveminds.smartcity.base.BaseViewModel;
import com.positiveminds.smartcity.data.Constants;
import com.positiveminds.smartcity.data.remote.HealthRecordService;
import com.positiveminds.smartcity.data.remote.ParkingSlotService;
import com.positiveminds.smartcity.exception.AppException;
import com.positiveminds.smartcity.health.HealthRecyclerViewAdapter;
import com.positiveminds.smartcity.health.model.HealthRecord;
import com.positiveminds.smartcity.server.RestManager;
import com.positiveminds.smartcity.transport.parking.ParkingRecyclerViewAdapter;
import com.positiveminds.smartcity.transport.parking.ParkingSlotNavigator;
import com.positiveminds.smartcity.transport.parking.ParkingSlotListObservable;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotData;
import com.positiveminds.smartcity.transport.parking.model.ParkingSlotResponse;
import com.positiveminds.smartcity.utils.rx.SchedulerProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class ParkingSlotViewModel extends BaseViewModel<ParkingSlotNavigator> {


    private ParkingSlotListObservable parkingSlotListObservable;

    public ParkingSlotViewModel(SchedulerProvider schedulerProvider) {
        super(schedulerProvider);
        parkingSlotListObservable = new ParkingSlotListObservable();
    }


    public ParkingSlotListObservable getParkingSlotListObservable() {
        return parkingSlotListObservable;
    }

    public void getParkingSlotsFromCloud(Location location) {
        if (location == null)
            return;
        setIsLoading(true);
        double lat = location.getLatitude();
        double lng = location.getLongitude();
        ParkingSlotService slotService = RestManager.getRetrofitClient().create(ParkingSlotService.class);

        Disposable disposable = slotService.getParkingSlotList("1", lat, lng).subscribeOn(getSchedulerProvider().io()).
                observeOn(getSchedulerProvider().ui()).subscribe(new Consumer<ParkingSlotResponse>() {
            @Override
            public void accept(ParkingSlotResponse parkingSlotResponse) throws Exception {
                setIsLoading(false);
                if (parkingSlotResponse == null) {
                    getAppExceptionEvent().setValue(new AppException(R.string.msg_no_response));
                    return;
                }

//                List<ParkingSlotData> parkingSlotDataList = new ArrayList<>();
//                ParkingSlotData parkingSlotData = new ParkingSlotData();
//                parkingSlotData.setName("Vaishali Metro");
//                parkingSlotData.setAddress("plot No.56, Vaishali");
//                parkingSlotData.setPrice(20);
//                parkingSlotDataList.add(parkingSlotData);
//                displayParkingSlots(parkingSlotDataList);

                boolean status = parkingSlotResponse.getStatus();
                if (!status) // true -> status
                {
                    String message = parkingSlotResponse.getMessage();
                    if (TextUtils.isEmpty(message))
                        message = Constants.RESPONSE_MSG_EMPTY;
                    getAppExceptionEvent().setValue(new AppException(message));
                    return;
                }
                displayParkingSlots(parkingSlotResponse.getParkingSlotDataList());

            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(Throwable throwable) throws Exception {
                setIsLoading(false);
                getAppExceptionEvent().setValue(new AppException(throwable.getMessage()));
            }
        });
        getCompositeDisposable().add(disposable);

    }


    //----------------------------------------------------------------------------------------------
    // BindingAdapters methods
    //----------------------------------------------------------------------------------------------
    @BindingAdapter({"app:adapter", "app:data"})
    public static void bind(RecyclerView recyclerView, ParkingRecyclerViewAdapter adapter,
                            List<ParkingSlotData> parkingSlotDataList) {
        recyclerView.setAdapter(adapter);
        // update adapter
        adapter.updateData(parkingSlotDataList);

    }

    private void displayParkingSlots(List<ParkingSlotData> parkingSlotDataList) {
        parkingSlotListObservable.setParkingSlotDataList(parkingSlotDataList);
    }


}
